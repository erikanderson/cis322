//(2)

//import java.math.BigInteger;
//
//public class Main {
//
//    public static void main(String[] args) {
//        long startTime = System.nanoTime();
//        BigInteger sum;
//        sum = new BigInteger("0");
//        BigInteger one;
//        one = new BigInteger("1");
//        int n = 100000;
//        for ( int i = 0; i < n; i++ )
//            for ( int j = 0; j < n; j++)
//                sum = sum.add(one);
//        long endTime = System.nanoTime();
//        System.out.println("Total execution time: " + (endTime - startTime) / 1000000);
//        System.out.println("Sum of these numbers: "+sum);
//    }
//}

//(4)
//import java.math.BigInteger;
//
//public class Main {
//
//    public static void main(String[] args) {
//        long startTime = System.nanoTime();
//        BigInteger sum;
//        sum = new BigInteger("0");
//        BigInteger one;
//        one = new BigInteger("1");
//        int n = 100000;
//        for ( int i = 0; i < n; i++ )
//            for ( int j = 0; j < i; j++)
//                sum = sum.add(one);
//        long endTime = System.nanoTime();
//        System.out.println("Total execution time: " + (endTime - startTime) / 1000000);
//        System.out.println("Sum of these numbers: "+sum);
//    }
//}

//(6)

//import java.math.BigInteger;
//
//public class Main {
//
//    public static void main(String[] args) {
//        long startTime = System.nanoTime();
//        BigInteger sum;
//        sum = new BigInteger("0");
//        BigInteger one;
//        one = new BigInteger("1");
//        int n = 1000;
//        for ( int i = 1; i < n; i++ )
//            for ( int j = 1; j < i * i; j++)
//                if ( j % i == 0 )
//                    for ( int k = 0; k < j; k++ )
//                        sum = sum.add(one);
//        long endTime = System.nanoTime();
//        System.out.println("Total execution time: " + (endTime - startTime) / 1000000);
//        System.out.println("Sum of these numbers: "+sum);
//    }
//}


//import java.math.BigInteger;
//
//public class Main {
//
//    public static void main(String[] args) {
//        long startTime = System.nanoTime();
//
//        sum = new BigInteger("0");
//        BigInteger one;
//        one = new BigInteger("1");
//        int n = 1000;
//        for ( int i = 1; i < n; i++ )
//            for ( int j = 1; j < i * i; j++)
//                if ( j % i == 0 )
//                    for ( int k = 0; k < j; k++ )
//                        sum = sum.add(one);
//        long endTime = System.nanoTime();
//        System.out.println("Total execution time: " + (endTime - startTime) / 1000000);
//        System.out.println("Sum of these numbers: "+sum);
//    }
//}

//Binary search algorithm

import java.util.Arrays;

public class Main
{
    public static final int NOT_FOUND = -1;

    /**
     * Performs the standard binary search.
     * @return index where item is found, or -1 if not found
     */
    public static <AnyType extends Comparable<? super AnyType>>
    int binarySearch( AnyType [ ] a, AnyType x )
    {

        int low = 0, high = a.length - 1;

        while( low <= high )
        {
            int mid = ( low + high ) / 2;

            if( a[ mid ].compareTo( x ) < 0 )
                low = mid + 1;
            else if( a[ mid ].compareTo( x ) > 0 )
                high = mid - 1;
            else

                return mid;   // Found
        }

        return NOT_FOUND;     // NOT_FOUND is defined as -1
    }

    // Test program
    public static void main( String [ ] args )
    {
        int SIZE = 10000000;
        Integer [ ] a = new Integer [ SIZE ];
        for( int i = 0; i < SIZE; i++ )
            a[ i ] = i * 2;
        int tosearch = 123123;
        //System.out.println(Arrays.toString(a));
        long startTime = System.nanoTime();
        int result;
        result = binarySearch( a, tosearch );
        long endTime = System.nanoTime();
        System.out.println("Start time: " + startTime + " End time: " + endTime);
        System.out.println("Total execution time finding item: " + (endTime - startTime));
        System.out.println( "Found " + tosearch + " at " + result );

    }
}

//class Main {
//    public static void main(String[] args) {
//        //System.out.println("Hello world!");
//        int sum = 0;
//        int n = 10;
//        for ( int i = 0; i < n; i++ )
//            for ( int j = 0; j < n; j++)
//                sum += 1;
//        int sum1 = sum;
//        return sum1;
//    }
//}


//public class Main {
//
//    public static void main(String[] args) {
//        System.out.println("Hello World!");
//        public static int sum (int n) {
//            int sum = 0;
//            for ( int i = 0; i < n; i++ )
//                for ( int j = 0; j < n; j++)
//                    sum += 1;
//            return sum;
//        }
//    }

    //sum = 0
    //  for ( i = 0; i < n; i++)
    //  for ( j = 0; j < n; j++)
    //sum++;


//    public static int sum (int n) {
//        int sum = 0;
//        for ( int i = 0; i < n; i++ )
//            for ( int j = 0; j < n; j++)
//                sum += 1;
//            return sum;
//    }

//
//
//public static int sum (int n) {
//    int sum = 0;
//    for ( int i = 0; i < n; i++ )
//        for ( int j = 0; j < n; j++)
//            sum += 1;
//    return sum;
//}}

