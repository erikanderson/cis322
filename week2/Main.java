//3.4
//1.	(3.4) Given two sorted lists, L1 and L2, write a procedure to compute L1∩L2 using
//        only the basic list operations.Analyze the time complexity of this procedure
//        in big-O notation, assuming the size of L1 is n and the size of L2 is n too.
//
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String args[]) {

        int n = 100000;
        LinkedList<Integer> linkedListOne = new LinkedList<Integer>();
        LinkedList<Integer> linkedListTwo = new LinkedList<Integer>();
        for (int i = 0; i < n; i++) {
            int randomNumOne = ThreadLocalRandom.current().nextInt(10, 100 + 1);
            linkedListOne.add(randomNumOne);
            int randomNumTwo = ThreadLocalRandom.current().nextInt(10, 100 + 1);
            linkedListTwo.add(randomNumTwo);
        }
        Collections.sort(linkedListOne);
        Collections.sort(linkedListTwo);
        System.out.println(linkedListOne);
        System.out.println(linkedListTwo);

        LinkedList<Integer> linkedListUnion = new LinkedList<Integer>();

        long startTime = System.nanoTime();

        Iterator<Integer> linkedListOneIterator = linkedListOne.iterator();
        while(linkedListOneIterator.hasNext()) {
            if (linkedListTwo.indexOf(linkedListOneIterator.next()) != -1) {
                linkedListUnion.add(linkedListOneIterator.next());
            }
        }

        long endTime = System.nanoTime();
        System.out.println("Total execution time: " + (endTime - startTime) / 1000000);


        System.out.println(linkedListUnion);
    }
}

// This will iterate over one list and for each item in L1 will search for it in L2. This operation is O(N^2) due to the nesting of the call.


//(3.22) Write a program to evaluate a postfix expression using one data structure
//        learned in this week. For help, read DSAA Page 85-86 “Postfix Expressions”.
//        Analyze the time complexity of this program in big-O notation, assuming the
//        total number of numbers and symbols in the expression is n.


//import java.security.SecureRandom;
//import java.util.Arrays;
//import java.util.Stack;
//
//public class Main {
//
//    public static void main(String[] args) {
//
//        String AB = "123456789+*";
//        SecureRandom rnd = new SecureRandom();
//        int len = 56;
//        StringBuilder sb = new StringBuilder( len );
//        for( int i = 0; i < len; i++ )
//            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
//        System.out.println(sb.toString());
//        //return sb.toString();
//
//        //randomString(23);
//
//        //String[] postfixExpression = {"4", "5", "*", "7", "+", "8", "*"};
//        String[] postfixExpression = {"6", "5", "2", "3", "+", "8", "*", "+", "3", "+", "*"};
//        //String[] postfixExpression = sb.toString().split(""); //{"6", "5", "2", "3", "+", "8", "*", "+", "3", "+", "*"};
//        System.out.println("postfixExpression");
//        System.out.println(Arrays.toString(postfixExpression));
//        Stack postfixStack = new Stack();
//
//        for (int i = 0; i < postfixExpression.length; i++) {
//            try {
//                int valToPush = Integer.parseInt(postfixExpression[i]);
//                //System.out.println("postfixStack");
//                //System.out.println(postfixStack);
//                postfixStack.push(valToPush);
//            } catch (NumberFormatException e) {
//                //Operator applied to the two numbers popped from the stack
//                Integer postfixNumOne = (int) postfixStack.pop();
//                Integer postfixNumTwo = (int) postfixStack.pop();
//                int res = 0;
//                if (postfixExpression[i].equals("+")) {
//                    res = postfixNumOne + postfixNumTwo;
//                    System.out.println(res);
//                    postfixStack.push(res);
//                } else if (postfixExpression[i].equals("*")) {
//                    res = postfixNumOne * postfixNumTwo;
//                    System.out.println(res);
//                    postfixStack.push(res);
//                } else if (postfixExpression[i].equals("-")) {
//                    res = postfixNumOne - postfixNumTwo;
//                    System.out.println(res);
//                    postfixStack.push(res);
//                } else if (postfixExpression[i].equals("/")) {
//                    res = postfixNumOne / postfixNumTwo;
//                    System.out.println(res);
//                    postfixStack.push(res);
//                } else {
//                    System.out.println("Operator not supported");
//                }
//                System.out.println(postfixExpression[i]);
//            }
//            System.out.println("postfixStack");
//            System.out.println(postfixStack);
//        }
//
//        System.out.println(postfixStack);
//    }
//}

// Was able to get the example from pg 85 to work and can conclude that this will run in O(N) time because "processing each element
// in the input consists of stack operations and thus takes constant time" (pg 87). In my code I think that I have introduced an
// inefficiency in the if conditional that is checking if the string is an operator but this should still result in O(N) time.
